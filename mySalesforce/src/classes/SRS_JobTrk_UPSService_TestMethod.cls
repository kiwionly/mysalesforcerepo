/**
 Description: Test method for SRS_JobTrk_UPSService
Business Owner: 

Dependencies: none 
Change History: 
Date       Person Responsible Details 
09/20/2012 kiwi  Created
 */
@isTest
private class SRS_JobTrk_UPSService_TestMethod 
{   
    static String digest = null;
    
    @isTest 
    static void testConfirmRequest()
    {
        SRS_JobTrk_UPSService ups = new SRS_JobTrk_UPSService();
        
        SRS_JobTrk_UPSService.Shipper shipper = new SRS_JobTrk_UPSService.Shipper();
        shipper.name = 'Seagate Recovery Services';
        shipper.phoneNumber = '18004750143';
        shipper.shipperNumber = '1W8V89';
        shipper.addressLine1 = '50 East Commerce Drive';
        shipper.addressLine2 = 'Suite 190';
        shipper.city = 'Schaumburg';
        shipper.stateCode = 'IL';
        shipper.countryCode = 'US';
        shipper.postalCode = '60173';
        
        SRS_JobTrk_UPSService.shipFrom shipFrom = new SRS_JobTrk_UPSService.shipFrom();
        shipFrom.companyName = 'Facebook Inc';
        shipFrom.addressLine1 = '1601 S. California Ave';
        shipFrom.city = 'Palo Alto';
        shipFrom.stateCode = 'CA';
        shipFrom.countryCode = 'US';
        shipFrom.postalCode = '94304';
        shipFrom.phoneNumber = '9725551212';
        
        SRS_JobTrk_UPSService.shipTo shipTo = new SRS_JobTrk_UPSService.shipTo();
        shipTo.companyName = 'Seagate Recovery Services';
        shipTo.addressLine1 = '50 East Commerce Drive';
        shipTo.addressLine1 = 'Suite 190';
        shipTo.phoneNumber = '18004750143';
        shipTo.city = 'Schaumburg';
        shipTo.stateCode = 'IL';
        shipTo.countryCode = 'US';
        shipTo.postalCode = '60173';
        
        SRS_JobTrk_UPSService.ShipmentPackage pac = new SRS_JobTrk_UPSService.ShipmentPackage();    
        pac.height = 1;
        pac.length = 1;
        pac.width = 1;
        pac.weight = 1; 
        pac.recoveryRequestId = '123';
        pac.customerId = '456'; 
        
        Map<String, String> confirmResult = ups.sendConfirmRequest(SRS_JobTrk_UPSService.Country.US_CA, shipper, shipFrom, shipTo, pac);
        
        String error = confirmResult.get('error');              
        digest = confirmResult.get('ShipmentDigest');
        
        System.assert(error == null);
        System.assert(digest != null);
    }
    
    @isTest 
    static void testAcceptRequest()
    {
        SRS_JobTrk_UPSService ups = new SRS_JobTrk_UPSService();
        
        Map<String, String> acceptResult = ups.sendAcceptRequest(digest);
                    
        String error = acceptResult.get('error');       
        String image = acceptResult.get('GraphicImage');
        
        System.debug('image found : ' + image);     
        
        System.assert(error == null);
        System.assert(image != null);
    }
    
    
}