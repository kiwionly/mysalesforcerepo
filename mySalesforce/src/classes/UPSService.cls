/*

UPSService : ups api client which use for shipping, method provided here only for confirm and accept shipment.


Shipper Numbers:

TORONTO: WE9457
Santa Clara: F7829V
Chicago: 1W8V89


*/
public class UPSService
{
	public enum Country {US_CA, MX, EMEA}
		
	private Http http;
	private HttpRequest req;
	
	private String token = '6BEC1BE08CECF660';
	private String user = 'fcrombeen';
	private String password = 'frankfrank';
		
	//note : this for test server, not for production use
	private String CONFIRM_URL = 'https://wwwcie.ups.com/ups.app/xml/ShipConfirm';
	private String ACCEPT_URL = 'https://wwwcie.ups.com/ups.app/xml/ShipAccept';
   
	public UPSService()
	{
		http = new Http();
		req = new HttpRequest();
	}	

	private HttpResponse send(String url, String body)
	{
		req.setEndpoint(url);
   		req.setMethod('POST');
   		req.setBody(body);
   		req.setTimeout(15000); 
   	
		HttpResponse res = http.send(req);
	
   		return res;
	}
	
	private Map<String, String> getElementTextByTagName(XmlStreamReader reader, String elementName)
	{
		Map<String, String> result = new Map<String, String>();
		
	    while(reader.hasNext())
		{
			if(reader.getEventType() == XmlTag.START_ELEMENT)
			{
				String name = reader.getLocalName();
				
				if(name.equals('ErrorDescription'))
				{
					String el = getElementText(reader);
					
					System.debug('error:' + el);
					
					result.put('error', el);
				}
				else if(name.equals(elementName))
				{
					String el = getElementText(reader);
					
					System.debug('get:' + elementName + ':' + el);	
					
					result.put(elementName, el);
				}
			}
			
			reader.next();
		}
		
		return result;
	}
	
	private String getElementText(XmlStreamReader reader)
	{		
		String text = ''; 
		
		while(reader.hasNext())
		{
			if(reader.getEventType() == XmlTag.END_ELEMENT)
			{	
				break;
			}
			else if(reader.getEventType() == XmlTag.CHARACTERS)
			{				
				text += reader.getText();
			}
						
			reader.next();
		}

		return text;
	}	
	
	public Map<String, String> sendConfirmRequest(Country code, Shipper shipper, ShipFrom shipFrom, ShipTo shipTo, ShipmentPackage pac)
	{		
		String confirmXML = null;
		
		if(code == Country.US_CA)
		{
			confirmXML = createConfirmXML(shipper, shipFrom, shipTo, pac);
		}		
		else if(code == Country.MX)
		{
			confirmXML = createConfirmXML(shipper, shipFrom, shipTo, pac); //change to Mexico
		}
		else if(code == Country.EMEA)
		{
			confirmXML = createConfirmXML(shipper, shipFrom, shipTo, pac); //change to EMEA
		}
		
		HttpResponse res = send(CONFIRM_URL, confirmXML);
		Map<String, String> result = getElementTextByTagName(res.getXmlStreamReader(), 'ShipmentDigest');
						
		return result;
	}
	
	public Map<String, String> sendAcceptRequest(String digest)
	{
		String acceptXML = createAcceptXML(digest);
		
		HttpResponse res = send(ACCEPT_URL, acceptXML);
		Map<String, String> result = getElementTextByTagName(res.getXmlStreamReader(), 'GraphicImage');

		return result;
	}
	
	private String createConfirmXML(Shipper shipper, ShipFrom shipFrom, ShipTo shipTo, ShipmentPackage pac)
	{		
		String xml = createXML() + 
		
'<?xml version="1.0"?>' + 
'<ShipmentConfirmRequest xml:lang="en-US">' + 
'	<Request>' + 
'		<TransactionReference>' + 
'			<CustomerContext>ShipConfirm</CustomerContext>' + 
'			<XpciVersion>1.0001</XpciVersion>' + 
'		</TransactionReference>' + 
'		<RequestAction>ShipConfirm</RequestAction>' + 
'		<RequestOption>nonvalidate</RequestOption>' + 
'	</Request>' + 
'	<LabelSpecification>' + 
'		<LabelPrintMethod>' + 
'			<Code>GIF</Code>' + 
'		</LabelPrintMethod>' + 
'		<LabelImageFormat>' + 
'			<Code>GIF</Code>' + 
'		</LabelImageFormat>' + 
'	</LabelSpecification>' + 
'	<Shipment>' + 
'		<Shipper>' + 
'			<Name>' + shipper.name + '</Name>' + 
'			<AttentionName>Seagate Recovery Services</AttentionName>	' + 
'			<PhoneNumber>' + shipper.phoneNumber + '</PhoneNumber>' + 
'			<ShipperNumber>' + shipper.shipperNumber + '</ShipperNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipper.addressLine1 + '</AddressLine1>' + 
'				<AddressLine2>' + shipper.addressLine2 + '</AddressLine2>' + 
'				<AddressLine3>' + shipper.addressLine3 + '</AddressLine3>' + 
'				<City>' + shipper.city + '</City>' + 
'				<StateProvinceCode>' + shipper.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipper.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipper.countryCode + '</CountryCode>' + 
'			</Address>' + 
'		</Shipper>' + 
'		<ShipTo>' + 
'			<CompanyName>' + shipTo.companyName + '</CompanyName>' + 
'			<AttentionName>Seagate Recovery Services</AttentionName>' + 
'			<PhoneNumber>' + shipTo.phoneNumber + '</PhoneNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipTo.addressLine1 + '</AddressLine1>' + 
'				<AddressLine2>' + shipTo.addressLine2 + '</AddressLine2>' + 
'				<AddressLine3>' + shipTo.addressLine3 + '</AddressLine3>' + 
'				<City>' + shipTo.city + '</City>				' + 
'				<StateProvinceCode>' + shipTo.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipTo.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipTo.countryCode + '</CountryCode>' + 
'			</Address>			' + 
'		</ShipTo>' + 
'		<ShipFrom>' + 
'			<CompanyName>' + shipFrom.companyName + '</CompanyName>' + 
'			<AttentionName>' + shipFrom.contactName + '</AttentionName>' + 
'			<PhoneNumber>' + shipFrom.phoneNumber + '</PhoneNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipFrom.addressLine1 + '</AddressLine1>' + 
'				<City>' + shipFrom.city + '</City>' + 
'				<StateProvinceCode>' + shipFrom.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipFrom.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipFrom.countryCode + '</CountryCode>' + 
'			</Address>' + 
'		</ShipFrom>' + 
'		<PaymentInformation>' + 
'			<Prepaid>' + 
'				<BillShipper>' + 
'					<AccountNumber>' + shipper.shipperNumber + '</AccountNumber>' + 
'				</BillShipper>' + 
'			</Prepaid>' + 
'		</PaymentInformation>' + 
'		<Service>' + 
'			<Code>' + pac.serviceCode + '</Code>' + 
'		</Service>' + 
'		<Package>' + 
'			<PackagingType>' + 
'				<Code>' + pac.typeCode + '</Code>' + 
'			</PackagingType>' + 
'			<Dimensions>' + 
'				<UnitOfMeasurement>' + 
'					<Code>' + pac.unitCode + '</Code>' + 
'				</UnitOfMeasurement>' + 
'				<Length>' + pac.length + '</Length>' + 
'				<Width>' + pac.width + '</Width>' + 
'				<Height>' + pac.height + '</Height>' + 
'			</Dimensions>' + 
'			<PackageWeight>' + 
'				<Weight>' + pac.weight + '</Weight>' + 
'			</PackageWeight>' +
'		</Package>' + 
'	</Shipment>' + 
'</ShipmentConfirmRequest>';
		
		return xml;
	}
	
	private String createAcceptXML(String digest)
	{
		String xml = createXML() +
		
'<?xml version="1.0"?>' + 
'<ShipmentAcceptRequest>' + 
'	<Request>' + 
'		<TransactionReference>' + 
'			<CustomerContext>ShipConfirm</CustomerContext>' + 
'			<XpciVersion>1.0001</XpciVersion>' + 
'		</TransactionReference>' + 
'		<RequestAction>ShipAccept</RequestAction>' + 
'		<RequestOption>01</RequestOption>' + 
'	</Request>' + 
'	<ShipmentDigest>' + digest + '</ShipmentDigest>' + 
'</ShipmentAcceptRequest>';		

		return xml;
	}
	
	private String createXML()
	{
		String xml = '<?xml version="1.0"?>' +
							'<AccessRequest xml:lang="en-US">' +
								'<AccessLicenseNumber>' + token + '</AccessLicenseNumber>' +
								'<UserId>' + user + '</UserId>' + 
							'<Password>' + password + '</Password>' +
						'</AccessRequest>';		
		return xml;
	}
			
		
	public class Shipper
	{
		public String name = '';
		public String attentionName = '';
		public String phoneNumber = '';
		public String shipperNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipTo
	{
		public String companyName = '';
		public String phoneNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipFrom
	{
		public String companyName = '';
		public String contactName = '';
		public String phoneNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipmentPackage
	{
		public String serviceCode = '14';
		public String typeCode = '02';
		public String unitCode = 'IN';
		public Integer length = 0;
		public Integer width = 0;
		public Integer height = 0;
		public Integer weight = 0;
	}
	
	public void testUPS()
	{		
		UPSService.Shipper shipper = new UPSService.Shipper();
		shipper.name = 'Seagate Technology';
		shipper.phoneNumber = '9725551212';
		shipper.shipperNumber = '1W8V89';
		shipper.addressLine1 = '1000 Preston Rd';
		shipper.city = 'Plano';
		shipper.stateCode = 'TX';
		shipper.countryCode = 'US';
		shipper.postalCode = '75093';
		
		UPSService.shipFrom shipFrom = new UPSService.shipFrom();
		shipFrom.companyName = 'Facebook Inc';
		shipFrom.addressLine1 = '1601 S. California Ave';
		shipFrom.city = 'Palo Alto';
		shipFrom.stateCode = 'CA';
		shipFrom.countryCode = 'US';
		shipFrom.postalCode = '94304';
		shipFrom.phoneNumber = '9725551212';
		
		UPSService.shipTo shipTo = new UPSService.shipTo();
		shipTo.companyName = 'Seagate Technology';
		shipTo.addressLine1 = '1000 Preston Rd';
		shipTo.phoneNumber = '9725551212';
		shipTo.city = 'Plano';
		shipTo.stateCode = 'TX';
		shipTo.countryCode = 'US';
		shipTo.postalCode = '75093';
		
		UPSService.ShipmentPackage pac = new UPSService.ShipmentPackage();	
		pac.height = 1;
		pac.length = 1;
		pac.width = 1;
		pac.weight = 1; 	
		
		Map<String, String> confirmResult = sendConfirmRequest(UPSService.Country.US_CA, shipper, shipFrom, shipTo, pac);
		
		String error = confirmResult.get('error');				
		String digest = confirmResult.get('ShipmentDigest');
		
		System.debug('error=' + error);
		System.debug('digest=' + digest);
		
		Map<String, String> acceptResult = sendAcceptRequest(digest);
					
		error = acceptResult.get('error');		
		String image = acceptResult.get('GraphicImage');
		
		System.debug('error=' + error);
		System.debug('image= ' + image);		
	}
	


}