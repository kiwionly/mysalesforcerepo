/*

SRS_JobTrk_UPSService : ups api client which use for shipping, method provided here only for confirm and accept shipment.

see testUPS() method below show how to use this class. It return a Map<String, String> object which could be 'error' or 'GraphicImage'.

this class using shipping APi from UPS. check the related document for more info.

Shipper Numbers:

TORONTO: WE9457
Santa Clara: F7829V
Chicago: 1W8V89

created by kiwi


*/
public class SRS_JobTrk_UPSService
{
	public enum Country {US_CA, MX, EMEA}
		
	private Http http;
	private HttpRequest req;
	
	private String token = '6BEC1BE08CECF660';
	private String user = 'fcrombeen';
	private String password = 'frankfrank';
		
	//note : this for test server, not for production use
	private String CONFIRM_URL = 'https://wwwcie.ups.com/ups.app/xml/ShipConfirm';
	private String ACCEPT_URL = 'https://wwwcie.ups.com/ups.app/xml/ShipAccept';
   
	public SRS_JobTrk_UPSService()
	{
		http = new Http();
		req = new HttpRequest();
	}	

	private HttpResponse send(String url, String body)
	{
		req.setEndpoint(url);
   		req.setMethod('POST');
   		req.setBody(body);
   		req.setTimeout(15000); 
   	
		HttpResponse res = http.send(req);
	
   		return res;
	}
	
	public Map<String, String> sendConfirmRequest(Country code, Shipper shipper, ShipFrom shipFrom, ShipTo shipTo, ShipmentPackage pac)
	{		
		String confirmXML = null;
		
		if(code == Country.US_CA)
		{
			pac.serviceCode = '01';
		}		
		else if(code == Country.MX)
		{
			pac.serviceCode = '07';
		}
		
		confirmXML = createConfirmXML(shipper, shipFrom, shipTo, pac);
		
		HttpResponse res = send(CONFIRM_URL, confirmXML);
		XmlParser parser = new XMLParser(res);

		Set<String> elementName = new Set<String>();
		elementName.add('errorDescription');
		elementName.add('ShipmentDigest');
		
		Map<String, String> result = parser.getElementTextByTagName(elementName);
						
		return result;
	}
	
	public Map<String, String> sendAcceptRequest(String digest)
	{
		String acceptXML = createAcceptXML(digest);
		
		HttpResponse res = send(ACCEPT_URL, acceptXML);
		XmlParser parser = new XMLParser(res);

		Set<String> elementName = new Set<String>();
		elementName.add('errorDescription');
		elementName.add('GraphicImage');
		
		Map<String, String> result = parser.getElementTextByTagName(elementName);

		return result;
	}
	
	private String createConfirmXML(Shipper shipper, ShipFrom shipFrom, ShipTo shipTo, ShipmentPackage pac)
	{		
		String xml = createXML() + 
		
'<?xml version="1.0"?>' + 
'<ShipmentConfirmRequest xml:lang="en-US">' + 
'	<Request>' + 
'		<TransactionReference>' + 
'			<CustomerContext>ShipConfirm</CustomerContext>' + 
'			<XpciVersion>1.0001</XpciVersion>' + 
'		</TransactionReference>' + 
'		<RequestAction>ShipConfirm</RequestAction>' + 
'		<RequestOption>nonvalidate</RequestOption>' + 
'	</Request>' + 
'	<LabelSpecification>' + 
'		<LabelPrintMethod>' + 
'			<Code>GIF</Code>' + 
'		</LabelPrintMethod>' + 
'		<LabelImageFormat>' + 
'			<Code>GIF</Code>' + 
'		</LabelImageFormat>' + 
'	</LabelSpecification>' + 
'	<Shipment>' +
'		<ReturnService>' + 
'			<Code>9</Code>' +
'		</ReturnService>' +
'		<Shipper>' +  
'			<Name>' + shipper.name + '</Name>' + 
'			<AttentionName>Seagate Recovery Services</AttentionName>	' + 
'			<PhoneNumber>' + shipper.phoneNumber + '</PhoneNumber>' + 
'			<ShipperNumber>' + shipper.shipperNumber + '</ShipperNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipper.addressLine1 + '</AddressLine1>' + 
'				<AddressLine2>' + shipper.addressLine2 + '</AddressLine2>' + 
'				<AddressLine3>' + shipper.addressLine3 + '</AddressLine3>' + 
'				<City>' + shipper.city + '</City>' + 
'				<StateProvinceCode>' + shipper.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipper.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipper.countryCode + '</CountryCode>' + 
'			</Address>' + 
'		</Shipper>' + 
'		<ShipTo>' + 
'			<CompanyName>' + shipTo.companyName + '</CompanyName>' + 
'			<AttentionName>' + shipTo.companyName + '</AttentionName>' + 
'			<PhoneNumber>' + shipTo.phoneNumber + '</PhoneNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipTo.addressLine1 + '</AddressLine1>' + 
'				<AddressLine2>' + shipTo.addressLine2 + '</AddressLine2>' + 
'				<AddressLine3>' + shipTo.addressLine3 + '</AddressLine3>' + 
'				<City>' + shipTo.city + '</City>				' + 
'				<StateProvinceCode>' + shipTo.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipTo.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipTo.countryCode + '</CountryCode>' + 
'			</Address>			' + 
'		</ShipTo>' + 
'		<ShipFrom>' + 
'			<CompanyName>' + shipFrom.companyName + '</CompanyName>' + 
'			<AttentionName>' + shipFrom.contactName + '</AttentionName>' + 
'			<PhoneNumber>' + shipFrom.phoneNumber + '</PhoneNumber>' + 
'			<Address>' + 
'				<AddressLine1>' + shipFrom.addressLine1 + '</AddressLine1>' + 
'				<City>' + shipFrom.city + '</City>' + 
'				<StateProvinceCode>' + shipFrom.stateCode + '</StateProvinceCode>' + 
'				<PostalCode>' + shipFrom.postalCode + '</PostalCode>' + 
'				<CountryCode>' + shipFrom.countryCode + '</CountryCode>' + 
'			</Address>' + 
'		</ShipFrom>' + 
'		<PaymentInformation>' + 
'			<Prepaid>' + 
'				<BillShipper>' + 
'					<AccountNumber>' + shipper.shipperNumber + '</AccountNumber>' + 
'				</BillShipper>' + 
'			</Prepaid>' + 
'		</PaymentInformation>' + 
'		<Service>' + 
'			<Code>' + pac.serviceCode + '</Code>' + 
'		</Service>' + 
'		<Package>' +
'			<ReferenceNumber>' + 
'				<Value>' + pac.recoveryRequestId + '</Value>' + 
'			</ReferenceNumber>' + 
'			<ReferenceNumber>' + 
'				<Value>' + pac.customerId + '</Value>' + 
'			</ReferenceNumber>' + 
'			<PackagingType>' + 
'				<Code>' + pac.typeCode + '</Code>' + 
'			</PackagingType>' + 
'			<Dimensions>' + 
'				<UnitOfMeasurement>' + 
'					<Code>' + pac.unitCode + '</Code>' + 
'				</UnitOfMeasurement>' + 
'				<Length>' + pac.length + '</Length>' + 
'				<Width>' + pac.width + '</Width>' + 
'				<Height>' + pac.height + '</Height>' + 
'			</Dimensions>' + 
'			<PackageWeight>' + 
'				<Weight>' + pac.weight + '</Weight>' + 
'			</PackageWeight>' +
'			<Description>Package</Description>' +
'		</Package>' + 
'		<Description>Package Description</Description>' +
'	</Shipment>' + 
'</ShipmentConfirmRequest>';
		
		return xml;
	}
	
	private String createAcceptXML(String digest)
	{
		String xml = createXML() +
		
'<?xml version="1.0"?>' + 
'<ShipmentAcceptRequest>' + 
'	<Request>' + 
'		<TransactionReference>' + 
'			<CustomerContext>ShipConfirm</CustomerContext>' + 
'			<XpciVersion>1.0001</XpciVersion>' + 
'		</TransactionReference>' + 
'		<RequestAction>ShipAccept</RequestAction>' + 
'		<RequestOption>01</RequestOption>' + 
'	</Request>' + 
'	<ShipmentDigest>' + digest + '</ShipmentDigest>' + 
'</ShipmentAcceptRequest>';		

		return xml;
	}
	
	private String createXML()
	{
		String xml = '<?xml version="1.0"?>' +
							'<AccessRequest xml:lang="en-US">' +
								'<AccessLicenseNumber>' + token + '</AccessLicenseNumber>' +
								'<UserId>' + user + '</UserId>' + 
							'<Password>' + password + '</Password>' +
						'</AccessRequest>';		
		return xml;
	}
			
		
	public class Shipper
	{
		public String name = '';
		public String attentionName = '';
		public String phoneNumber = '';
		public String shipperNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipTo
	{
		public String companyName = '';
		public String phoneNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipFrom
	{
		public String companyName = '';
		public String contactName = '';
		public String phoneNumber = '';
		public String addressLine1 = '';
		public String addressLine2 = '';
		public String addressLine3 = '';
		public String city = '';
		public String stateCode = '';
		public String postalCode = '';
		public String countryCode = '';
	}
	
	public class ShipmentPackage
	{
		public String serviceCode = '01';
		public String typeCode = '02';
		public String unitCode = 'IN';
		public Integer length = 0;
		public Integer width = 0;
		public Integer height = 0;
		public Integer weight = 0;
		public String recoveryRequestId = '';
		public String customerId;
	}
	
	/*
		This code demostrate how to use SRS_JobTrk_UPSService class. 
		All the variable shouold be set to ensure correct xml.
		
		This method should be move to test class after winter 2012 start support callout.
		
		you call run this code in developer console:
		
			SRS_JobTrk_UPSService ups = new SRS_JobTrk_UPSService();
			ups.testUPS();
			
	*/	
	public void testUPS()
	{		
		SRS_JobTrk_UPSService.Shipper shipper = new SRS_JobTrk_UPSService.Shipper();
		shipper.name = 'Seagate Recovery Services';
		shipper.phoneNumber = '18004750143';
		shipper.shipperNumber = '1W8V89';
		shipper.addressLine1 = '50 East Commerce Drive';
		shipper.addressLine2 = 'Suite 190';
		shipper.city = 'Schaumburg';
		shipper.stateCode = 'IL';
		shipper.countryCode = 'US';
		shipper.postalCode = '60173';
		
		SRS_JobTrk_UPSService.shipFrom shipFrom = new SRS_JobTrk_UPSService.shipFrom();
		shipFrom.companyName = 'Facebook Inc';
		shipFrom.addressLine1 = '1601 S. California Ave';
		shipFrom.city = 'Palo Alto';
		shipFrom.stateCode = 'CA';
		shipFrom.countryCode = 'US';
		shipFrom.postalCode = '94304';
		shipFrom.phoneNumber = '9725551212';
		
		SRS_JobTrk_UPSService.shipTo shipTo = new SRS_JobTrk_UPSService.shipTo();
		shipTo.companyName = 'Seagate Recovery Services';
		shipTo.addressLine1 = '50 East Commerce Drive';
		shipTo.addressLine1 = 'Suite 190';
		shipTo.phoneNumber = '18004750143';
		shipTo.city = 'Schaumburg';
		shipTo.stateCode = 'IL';
		shipTo.countryCode = 'US';
		shipTo.postalCode = '60173';
		
		SRS_JobTrk_UPSService.ShipmentPackage pac = new SRS_JobTrk_UPSService.ShipmentPackage();	
		pac.height = 1;
		pac.length = 1;
		pac.width = 1;
		pac.weight = 1; 
		pac.recoveryRequestId = '123';
		pac.customerId = '456';	
		
		Map<String, String> confirmResult = sendConfirmRequest(SRS_JobTrk_UPSService.Country.US_CA, shipper, shipFrom, shipTo, pac);
		
		String error = confirmResult.get('error');				
		String digest = confirmResult.get('ShipmentDigest');
		
		System.debug('error=' + error);
		System.debug('digest=' + digest);
		
		Map<String, String> acceptResult = sendAcceptRequest(digest);
					
		error = acceptResult.get('error');		
		String image = acceptResult.get('GraphicImage');
		
		System.debug('error=' + error);
		System.debug('image=' + image);		
	}
	


}