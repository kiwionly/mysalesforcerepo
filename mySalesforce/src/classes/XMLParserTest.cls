@isTest 
private class XMLParserTest
{
	@isTest 
    static void testGetElementTextbyTagName() 
    {
    	String xml = '<books>' +
        			 	'<book author="Sassy">Baz</book>' +
        			 	'<ebook author="kiki">wakiki</ebook>' +
        			 '</books>';
    	
    	XMLParser parser = new XMLParser(xml);
    	
		Set<String> elementName = new Set<String>();
		elementName.add('book');
		elementName.add('ebook');
		
		//return key pair value of result
		Map<String, String> result = parser.getElementTextByTagName(elementName);
		
		String book = result.get('book');
		System.assertEquals('Baz', book);
		
		String ebook = result.get('ebook');
		System.assertEquals('wakiki', ebook);
		
		System.debug(book);
		System.debug(ebook);
    }
}