public class SRS_JobTrk_PrintLabelController
{
    public String getLabel()
    {  
        SRS_JobTrk_UPSService ups = new SRS_JobTrk_UPSService();
    
        SRS_JobTrk_UPSService.Shipper shipper = new SRS_JobTrk_UPSService.Shipper();
        shipper.name = 'Seagate Recovery Services';
        shipper.phoneNumber = '18004750143';
        shipper.shipperNumber = '1W8V89';
        shipper.addressLine1 = '50 East Commerce Drive';
        shipper.addressLine2 = 'Suite 190';
        shipper.city = 'Schaumburg';
        shipper.stateCode = 'IL';
        shipper.countryCode = 'US';
        shipper.postalCode = '60173';
        
        SRS_JobTrk_UPSService.shipFrom shipFrom = new SRS_JobTrk_UPSService.shipFrom();
        shipFrom .companyName = 'Seagate Recovery Services';
        shipFrom .addressLine1 = '50 East Commerce Drive';
        shipFrom .addressLine2 = 'Suite 190';
        shipFrom .phoneNumber = '18004750143';
        shipFrom .city = 'Schaumburg';
        shipFrom .stateCode = 'IL';
        shipFrom .countryCode = 'US';
        shipFrom .postalCode = '60173';
        
        SRS_JobTrk_UPSService.shipTo shipTo = new SRS_JobTrk_UPSService.shipTo();
        shipTo.companyName = 'Facebook Inc';
        shipTo.addressLine1 = '1601 S. California Ave';
        shipTo.city = 'Palo Alto';
        shipTo.stateCode = 'CA';
        shipTo.countryCode = 'US';
        shipTo.postalCode = '94304';
        shipTo.phoneNumber = '9725551212';       
        
        SRS_JobTrk_UPSService.ShipmentPackage pac = new SRS_JobTrk_UPSService.ShipmentPackage();    
        pac.height = 1;
        pac.length = 1;
        pac.width = 1;
        pac.weight = 1; 
        pac.recoveryRequestId = '123';
        pac.customerId = '456'; 
        
        Map<String, String> confirmResult = ups.sendConfirmRequest(SRS_JobTrk_UPSService.Country.US_CA, shipper, shipFrom, shipTo, pac);
        
        String error = confirmResult.get('error');              
        String digest = confirmResult.get('ShipmentDigest');        
        
        Map<String, String> acceptResult = ups.sendAcceptRequest(digest);
                    
        error = acceptResult.get('error');      
        String image = acceptResult.get('GraphicImage');
                
        return image;                
    }
}