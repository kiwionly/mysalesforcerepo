public class MyController 
{
	public String name {get;set;}
	public String message {get; private set;}
	
	public PageReference hello()
	{
		message = 'hello ' + name;
		
		return null;
	}
}