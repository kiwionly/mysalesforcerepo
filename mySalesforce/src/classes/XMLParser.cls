/*

XMLParser is use for reading value from xml.

	XMLParser p = new XMLParser();
	Set<String> set = new Set<String>();
	set.add('name');
	set.add('value');
	
	//return key pair value of result
	Map<String, String> result = p.getElementTextByTagName(set);

*/
public class XMLParser 
{
	private Xmlstreamreader reader;
	
	public XMLParser(String str)
	{
		reader = new XmlStreamReader(str); 
	}
	
	public XMLParser(HttpResponse res)
	{
		reader = res.getXmlStreamReader();
	}
	
	public XMLParser(Xmlstreamreader re)
	{
		reader = re;
	}
	
	public Map<String, String> getElementTextByTagName(Set<String> elementName)
	{
		Map<String, String> result = new Map<String, String>();
		
	    while(reader.hasNext())
		{
			if(reader.getEventType() == XmlTag.START_ELEMENT)
			{
				String name = reader.getLocalName();
				
				for(String eleName:elementName) 
				{
					if(name.equals(eleName))
					{
						String el = getElementText();
						
						System.debug('get:' + eleName + ':' + el);	
						
						if(result.get(eleName) == null)
						{
							result.put(eleName, el);
						}
					}
				}
			}
			
			reader.next();
		}
		
		return result;
	}
	
	private String getElementText()
	{		
		String text = ''; 
		
		while(reader.hasNext())
		{
			if(reader.getEventType() == XmlTag.END_ELEMENT)
			{	
				break;
			}
			else if(reader.getEventType() == XmlTag.CHARACTERS)
			{				
				text += reader.getText();
			}
						
			reader.next();
		}

		return text;
	}
}