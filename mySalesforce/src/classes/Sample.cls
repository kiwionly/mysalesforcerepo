public class Sample {
 
    // Static variable that assumes a test is not running
    public static boolean isApexTest = false;
     
    public String main(){
         
        // Do a whole bunch of stuff
         
        // Build the http request
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://local.yahooapis.com/MapsService/V1/geocode?appid=YD-9G7bey8_JXxQP6rxl.fBFGgCdNjoDMACQA--&street=701+First+Ave&city=Sunnyvale&state=CA'); 
        req.setMethod('GET');
         
        // Invoke web service call
        String result = '';
        if (!isApexTest){
            // Make a real callout since we are not running a test
            HttpResponse res = h.send(req);
            result = res.getBody();
        } else {
            // A test is running
            result = '<?xml version="1.0"?><ResultSet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:yahoo:maps" xsi:schemaLocation="urn:yahoo:maps http://api.local.yahoo.com/MapsService/V1/GeocodeResponse.xsd"><Result precision="address"><Latitude>37.416397</Latitude><Longitude>-122.025055</Longitude><Address>701 1st Ave</Address><City>Sunnyvale</City><State>CA</State><Zip>94089-1019</Zip><Country>US</Country></Result></ResultSet>';
        }
             
        // Do whole bunch of stuff
         
        return result;      
    }
     
    // Wrapper method for "main" that we will call in the Test Class
    public String mainForTest(){
        isApexTest = true;
        return main();
    }
}